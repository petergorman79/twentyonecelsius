<?php 

get_header(); ?>

<!-- Page Title -->
<div class="section-fullwidth page-title">
  <div class="container">

    <div class="row">
      <div class="col-sm-12">
        <h1>404. Page not found</h1>
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container --> 
</div>
<!-- /Page Title -->

<div class="container">
  <div class="row">
    <section class="col-md-8 body-content">
      <!-- article -->
			<article id="post-404">

				<h4>
					We could not find this page on our servers, please <a href="<?php echo home_url(); ?>" title="Return home">click here</a> to return to our Home Page.
				</h4>

				<a href="<?php echo home_url(); ?>" class="cta cta-lg pri" title="Return home"><span>Return home</span></a>

			</article>
			<!-- /article -->
    </section> <!-- /Col -->

    <?php get_sidebar(); ?>

  </div> <!-- /Row -->
  <?php include('sidebar-foot.php'); ?>
</div>



<?php get_footer(); ?>
