$( document ).ready(function() {

  //Mobile menu
  $("#js_myMenu").mmenu();
  $("#js_myButton").click(function() {
     $("#js_myMenu").trigger("open.mm");
  });

  //Quick links menu
  $("#js_quickLinks").mmenu({
     "offCanvas": {
        "position": "right",
        "zposition": "front",
        "pageNodetype": "aside"
     },
     "header": {
        "title": "Quick Links",
        "add": true,
        "update": true
     }
  });
  $("#js_quickButton").click(function() {
     $("#js_quickLinks").trigger("open.mm");
     $("#js_quickLinks_close").trigger("close.mm");
  });

  //Sticky main menu
  $('#js-sticky').waypoint('sticky');

  //Testimonial slider
  $('.bxslider').bxSlider({
  	auto: true,
  	speed: 700,
  	pause: 6000,
  	randomStart: true,
  	pager: false
  });

  //Show call button on mobile devices. 
  if( /Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

    (function() {
      var node = document.createElement('style');
      document.body.appendChild(node);
      window.addStyleString = function(str) {
          node.innerHTML = str;
      };
    }());

    addStyleString('.mobile-call { display:inline-block }');

  }


	// Google API
	(function($) {

	/*
	*  render_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$el (jQuery element)
	*  @return	n/a
	*/

	function render_map( $el ) {

		// var
		var $markers = $el.find('.marker');

		// vars
		var args = {
			zoom		: 12,
			scrollwheel: false,
			draggable: true,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};

		// create map	        	
		var map = new google.maps.Map( $el[0], args);

		// add a markers reference
		map.markers = [];

		// add markers
		$markers.each(function(){

	    	add_marker( $(this), map );

		});

		// center map
		center_map( map );

	}

	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function add_marker( $marker, map ) {

		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

				infowindow.open( map, marker );

			});
		}

	}

	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function center_map( map ) {

		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

			bounds.extend( latlng );

		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 12 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}

	}

	$('.acf-map').each(function(){

		render_map( $(this) );

	});
	})(jQuery);


	//Gravity Forms Inline validation

	// if an invalid form field has been made valid,
	// remove the shouty error highlighting - if a valid
	// required field has been made invalid, start shouting

	$('input, textarea, select').focus(function(){
	    var $input     = $(this);
	    var isRequired = $input.parents('.gfield').is('.gfield_contains_required');
	    var isValid    = $input.is(':valid');

	    if ( isRequired && isValid ) {
	        $input.parents('.gfield').removeClass('gfield_error');
	        $input.parent().next('.validation_message').slideUp();
	    }

	}).blur(function(){
	    var $input     = $(this);
	    var isRequired = $input.parents('.gfield').is('.gfield_contains_required');
	    var isInValid  = $input.is(':invalid');
	    var isEmpty    = $input.val() === '';
	    var isValid    = $input.is(':valid');

	    if ( isRequired && ( isInValid || isEmpty ) ) {
	        $input.parents('.gfield').addClass('gfield_error');
	        $input.parent().next('.validation_message').slideDown();
	    } else if ( isRequired && isValid ) {
	    	$input.parents('.gfield').removeClass('gfield_error');
	    	$input.parents('.gfield').addClass('gfield_allgood');
	    }
	});

});

