<footer class="section-fullwidth sub-section" role="contentinfo">
  <div class="container">

    <div class="row">
      <div class="col-xs-12 col-md-8">
        <nav>
          <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
        </nav>
      </div> <!-- /Col -->
      <div class="col-xs-12 col-md-4">
        <div class="call-us hidden-xs hidden-sm">
          <?php the_field('call_now', 'option'); ?>
        </div>
      </div>
      <div class="col-xs-12">
        <hr>
      </div> <!-- /Col -->
      <div class="col-xs-12 col-md-8">
        
        <nav>
          <p class="company-footer">&copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?></p>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
        </nav>
      </div> <!-- /Col -->
      <div class="col-xs-12 col-md-4">
        <ul class="social-badges">
          <li>Stay in touch</li>
          <?php

          // check if the repeater field has rows of data
          if( have_rows('social_bar', 'option') ):

            // loop through the rows of data
            while ( have_rows('social_bar', 'option') ) : the_row(); ?>

          <li><a href="<?php the_sub_field('social_link', 'option'); ?>" target="_blank" title="<?php the_sub_field('social_name', 'option'); ?>"><i class="fa <?php the_sub_field('social_icon', 'option'); ?>"></i></a></li>

            <?php endwhile;

          else :

          endif; ?>

        </ul>
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container -->
</footer>
</div> <!-- /Wrapper -->

<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/functions.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<!-- Google Code for Contact Enquiry Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957262894;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NqLbCJXU1FcQrti6yAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/957262894/?label=NqLbCJXU1FcQrti6yAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957262894;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/957262894/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


</body>
</html>
