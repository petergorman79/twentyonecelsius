<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title>
  <?php
  if ( is_front_page() ) {
    bloginfo('name');
  } else {
    wp_title('');
  }
  ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
  <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.png" />
  <!-- For iPhone 4 Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon-114x114-precomposed.png">
  <!-- For iPad: -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon-72x72-precomposed.png">
  <!-- For iPhone: -->
  <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon-57x57-precomposed.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/css/bootstrap.min.css" />
  <link href="<?php echo get_template_directory_uri(); ?>/assets/plugins/jQuery.mmenu-master/src/css/jquery.mmenu.all.css" type="text/css" rel="stylesheet" />
  <link href="<?php echo get_template_directory_uri(); ?>/assets/plugins/bxslider/jquery.bxslider.css" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/styles.css" />

	<?php wp_head(); ?>

	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-2.1.1.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/plugins/jQuery.mmenu-master/src/js/jquery.mmenu.min.all.js" type="text/javascript"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/plugins/waypoints/waypoints.min.js" type="text/javascript"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/plugins/waypoints/waypoints-sticky.min.js" type="text/javascript"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/plugins/bxslider/jquery.bxslider.min.js" type="text/javascript"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37096829-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body <?php body_class(); ?>>

<div class="wrapper">

<div id="js-sticky" class="sticky-header-nav">
<div class="section-fullwidth header">

<header class="container" role="banner">

	<div class="row">
		<div class="col-sm-12">

      <div class="row">
        <div class="col-xs-3">
          <div class="logo"><a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><h1 class="text-hide" style="background-image:url('<?php the_field('company_logo', 'option'); ?>');"><?php bloginfo('name'); ?></h1></a></div>
        </div> <!-- /Col -->
        <div class="col-xs-9">
          <div class="call-us hidden-xs hidden-sm">
            <?php the_field('call_now', 'option'); ?>
          </div>

          <nav class="hidden-xs hidden-sm button" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'focus-menu' ) ); ?>
          </nav>
          <a id="js_myButton" href="#js_myMenu" class="mobile-nav-toggle visible-xs visible-sm">Menu <i class="fa fa-bars"></i></a>
        </div> <!-- /Col -->
      </div> <!-- /Row -->
      
		</div> <!-- /Col -->
	</div> <!-- /Row -->

</header>

</div> <!-- /Section Fullwidth -->
</div> <!-- /Sticky -->

<!-- Mobile Menu -->
<div class="mobile-nav visible-xs visible-sm">
  <nav id="js_myMenu" role="navigation">
    <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
  </nav>
</div>
<!-- /Mobile Menu -->
