<?php
/*
Template Name: Homepage
*/

get_header(); ?>
<main role="main">

<!-- Hero Unit -->
<div class="hero-unit section-fullwidth" style="background-image: url(<?php the_field('hero_image'); ?>);">
  <div class="container hero-content">

    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_field('hero_title'); ?></h1>
        <div class="well">
          <h2><?php the_field('hero_subtitle'); ?></h2>
          <a href="<?php the_field('hero_button_link'); ?>" class="cta cta-lg pri" title="<?php the_field('hero_button_label'); ?>"><span><?php the_field('hero_button_label'); ?></span></a>
          <a href="tel:<?php the_field('call_now'); ?>" class="cta cta-lg pri mobile-call" title="Call now <?php the_field('call_now'); ?>"><span>Call now <?php the_field('call_now', 'option'); ?></span></a>
        </div>
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container --> 
</div>
<!-- /Hero Unit -->

<!-- Services -->
<div class="section-fullwidth section-strips">
<section class="container services sub-section">

  <div class="row">
		<?php

		// check if the repeater field has rows of data
		if( have_rows('services_loop') ):

			// loop through the rows of data
		  while ( have_rows('services_loop') ) : the_row(); ?>

			<div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0 service-modules">
	      <a href="<?php the_sub_field('button_link'); ?>" title="<?php the_sub_field('service_title'); ?>">

					<?php $image = get_sub_field('service_image'); ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	        <h3><?php the_sub_field('service_title'); ?></h3>
	        <?php the_sub_field('service_description'); ?>
	      </a>
	      <a href="<?php the_sub_field('button_link'); ?>" title="<?php the_sub_field('cta_label'); ?>" class="cta cta-sm pri"><?php the_sub_field('cta_label'); ?></a>
	    </div> <!-- /Col -->

		  <?php endwhile;

		else :

		endif; ?>
  </div> <!-- /Row -->

</section>
</div>
<!-- /Services -->

<!-- Testimonials -->
<div class="section-fullwidth testimonial-bg">
<section class="container testimonials sub-section">
  <div class="row">
    <div class="col-xs-12">
      <h2>What people are saying about TwentyOne Celsius</h2>
      <div class="testimonial-slider">
        <ul class="bxslider">
        <?php

				// check if the repeater field has rows of data
				if( have_rows('testimonial_loop') ):

					// loop through the rows of data
				  while ( have_rows('testimonial_loop') ) : the_row(); ?>

					<li>
            <div class="testimonial-box">
              <?php the_sub_field('testimonial_content'); ?>
            </div>
          </li>

				  <?php endwhile;

				else :

				endif; ?>

        </ul>
      </div>
    </div> <!-- /Col -->
  </div> <!-- /Row -->
</section>
</div>
<!-- /Testimonials -->

<!-- CTA -->
<div class="section-fullwidth section-strips">
<section class="container last-call sub-section">

  <div class="row">
    <div class="col-xs-12">
      <a href="<?php the_field('hero_button_link'); ?>" class="cta cta-lg pri" title="<?php the_field('hero_button_label'); ?>"><span><?php the_field('hero_button_label'); ?></span></a>
    </div> <!-- /Col -->
  </div> <!-- /Row -->

</section>
</div>
<!-- /CTA -->

</main>

<?php get_footer(); ?>
