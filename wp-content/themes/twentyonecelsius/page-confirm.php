<?php

/* Template Name: Confirm Template */

get_header(); ?>

<!-- Page Title -->
<div class="section-fullwidth page-title">
  <div class="container">

    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container --> 
</div>
<!-- /Page Title -->

<div class="container">
  <div class="row">
    <section class="col-md-8 body-content">
      <?php the_content(); ?>
    </section> <!-- /Col -->

    <?php get_sidebar(); ?>

  </div> <!-- /Row -->
  <?php include('sidebar-foot.php'); ?>
</div>



<?php get_footer('confirm'); ?>
