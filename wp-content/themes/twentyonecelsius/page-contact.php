<?php

/* Template Name: Contact Page */

get_header(); ?>

<!-- Page Title -->
<div class="section-fullwidth page-title">
  <div class="container">

    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container --> 
</div>
<!-- /Page Title -->

<!-- Page Hero -->
<div class="section-fullwidth maps-hero" style="display:<?php the_field('toggle_hero'); ?>;">

        
        <?php 

        $location = get_field('google_map');

        if( !empty($location) ):
        ?>
        <div class="acf-map">
          <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
        </div>
        <?php endif; ?>

</div>
<!-- /Page Hero -->

<div class="container">
  <div class="row">
    <section class="col-md-8 body-content">
      <?php the_content(); ?>
    </section> <!-- /Col -->

    <section class="col-md-4 body-content">
      <h3>Phone</h3>
      <h4><?php the_field('call_now', 'option'); ?></h4>
      <h3>Address</h3>
      <h4><?php the_field('address', 'option'); ?></h4>
      <h3>Email</h3>
      <h4><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></h4>
      <h3>Fax</h3>
      <h4><?php the_field('fax', 'option'); ?></h4>
      <hr>
      <p>For any industrial or commercial installation or controls enquiry including tender requests please contact Greg Costa directly on <strong>0401 358 870</strong> or <a href="mailto:greg@twentyonecelsius.com.au">greg@twentyonecelsius.com.au</a>.</p>
      
    </section> <!-- /Col -->

    <?php get_sidebar(); ?>

  </div> <!-- /Row -->
  <?php include('sidebar-foot.php'); ?>
</div>



<?php get_footer(); ?>
