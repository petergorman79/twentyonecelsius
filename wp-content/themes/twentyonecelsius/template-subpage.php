<?php

/* Template Name: Subpage Template */

get_header(); ?>

<!-- Page Title -->
<div class="section-fullwidth page-title">
  <div class="container">

    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container --> 
</div>
<!-- /Page Title -->

<!-- Page Hero -->
<div class="section-fullwidth page-hero" style="display:<?php the_field('toggle_hero'); ?>;">
  <div class="container">

    <div class="row">
      <div class="col-sm-8">
        <?php the_field('hero_description'); ?>
        <a href="<?php the_field('button_link'); ?>" class="cta cta-md pri" title="<?php the_field('button_label'); ?>"><span><?php the_field('button_label'); ?></span></a>
      </div> <!-- /Col -->

      <div class="col-sm-4 page-hero-img hidden-xs">
      	<?php $image = get_field('hero_image'); ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
      </div> <!-- /Col -->
    </div> <!-- /Row -->

  </div> <!-- /Container --> 
</div>
<!-- /Page Hero -->

<div class="container">
  <div class="row">
    <section class="col-md-8 body-content">
      <?php the_content(); ?>
    </section> <!-- /Col -->

    <?php get_sidebar(); ?>

  </div> <!-- /Row -->
  <?php include('sidebar-foot.php'); ?>
</div>



<?php get_footer(); ?>
